# Installing openCFS on Linux

We provide openCFS including all dependencies in a single archive.
The software should run on any recent linux system.

To install, just download the most recent archive, e.g. the recent master build: [CFS-master-Linux.tar.gz](https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz), and extract it to the desired location.

```shell
wget https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz
tar -xzvf CFS-master-Linux.tar.gz
```

This will extract to `CFS-<SHA>-Linux` where `<sha>` is the commit checksum of the installation.
The executables are found in the `bin` directory of the installation, e.g. in the directory `CFS-<SHA>-Linux/bin`.
You can add this directory to your `PATH` by running

```
export PATH=<absolute-cfs-installation-path>/bin:$PATH
```

To make this persistent add it to the configuration file of your shell, e.g. to `~/.basrc` for bash.

You can now run cfs by
```shell
cfs -h
```
