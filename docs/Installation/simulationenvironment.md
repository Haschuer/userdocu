# Setup the simulation environment for OpenCFS

Before you start using openCFS, you might want to install some pre- and postprocessing software, in case you didn't already do this beforehand.

All the following instructions are tested via Ubuntu 20.04 but they should at least work for 18.04 as well.

---

# Preprocessing
For meshing purposes, we either propose to use gmsh or trelis.
Gmsh is an opensource meshing tool, trelis a commercial one.

## Installing gmsh
Install it from the official package manager
```shell
sudo apt install gmsh
```
---

# XML Editing
You can either use the commercial xml editor oxygen or the free eclipse IDE with an additional xml plugin.

## Installing eclipse with xml plugin
First install eclipse
```shell
sudo apt install eclipse
```
Then open eclipse and go to Help->Eclipse Marketplace and search for "Eclipse XML Editors" as shown and install it.
Then restart eclipse.

![a1](xmleclipse.png)



## Activating xml validation in eclipse
You can either open your xml files right away or you can use the full xml validation and suggestion abilities, which requires to create a 'dummy xml project' by right-clickin gin the project expolrer and create a new xml project, as shown in the screenshots down below.

![a3](xml3.png)

![a2](xml2.png)

This creates a project, in which you can load the xml files.



The only thing left to do is to restart eclipse and then open your xml file.

For an instruction on how to edit an xml file, click [here](../Tutorials/XMLExplanations/editing.md)

To find out more about the structure of you scheme, visit [this](../Tutorials/XMLExplanations/structure.md) page.


