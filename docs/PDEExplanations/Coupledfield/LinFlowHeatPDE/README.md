## LinFlow-heat coupling

Sound propagation in small dimensions where the viscous and thermal boundary layers are comparable to the feature dimensions, not only is affected by the viscous losses ([LinFlow PDE](../../Singlefield/LinFlowPDE/README.md)) but also is affected by thermal losses. The thermal losses will be considered by coupling the [heat PDE](../../Singlefield/HeatPDE/README.md) to [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md). The coupling terms are discussed here.

### Coupling conditions
Considering the temperature variations in LinFlow PDE requires using the equation of state and applying the relations among pressure, density and temperature. openCFS uses the linearized equation of state a

\begin{equation}
\rho = \left(\frac{\partial \rho}{\partial p}\right)_{T_0} p + \left(\frac{\partial \rho}{\partial T}\right)_{p_0} T\\ 
= \rho_0 \left(\beta_\mathrm{T} p - \alpha_\mathrm {p} T\right)\,.
\label{eq:eq-of-state}
\end{equation}

Where $\beta_T$ and $\alpha_p$ are the isothermal coefficients of compressibility and the isobaric coefficient of thermal expansion, respectively. In general, these coefficients can be written in terms of the measurable fluid properties such as density, heat capacity, adiabatic exponent (heat capacity ratio), speed of sound, and reference temperature [@morse1986theoretical]. These coefficients are 

\begin{equation}
\alpha_\mathrm {p} =- \frac{1}{\rho_0}\left(\frac{\partial \rho}{\partial T}\right)_{p_0} = \frac{1}{c}\sqrt{\frac{c_\mathrm {p}(k-1)}{T_0}} \\
\beta_\mathrm {T} = \frac{1}{\rho_0} \left(\frac{\partial \rho}{\partial p}\right)_{T_0} = \frac{1}{\rho_0} \frac{k}{c^2},
\label{eq:alpha&beta}
\end{equation}

with the speed of sound $c = \sqrt{\frac{K}{\rho_0}}$, compression modulus $K$, specific heat capacity at constant pressure $c_{\rm p}$ and adiabatic exponent $k$ . Applying \eqref{eq:eq-of-state} in conservation of mass in [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) result in following

\begin{eqnarray}
\frac{k}{c^2} \frac{\partial p}{\partial t}
-\frac{\rho_0}{c}\sqrt{\frac{c_{\rm p}(k-1)}{T_0}} \frac{\partial T}{\partial t}
+\nabla \cdot (\rho_0 \mathbf v) = 0 \,.
\label{eq:massHeatCoupling}
\end{eqnarray}

On the other hand, applying \eqref{eq:eq-of-state} to the Heat PDE changes it 
 
\begin{equation}
\rho_0 c_\mathrm{p} \frac{\partial T}{\partial t} -\nabla \cdot (k \nabla T) 
- \frac{\partial p}{\partial t} = 0 \,.
\label{eq:energyacou}
\end{equation}

In the openCFS simulation-xml file, the LinFlow-heat coupling is defined as

```
        <couplingList>
            <direct>
                <linFlowHeatDirect>
                    <regionList>
                        <region  name=""/>
                    </regionList>
                </linFlowHeatDirect>     
            </direct>
        </couplingList>
```
and in the ```<pdeList>``` both LinFlow ``` fluidMechLin ```and heat PDE ```heatConduction``` should be defiend.

### Material
In addition to general heat conduction material parameters (density, heat capacity, and heat conductivity)
in LinFlow-heat PDE, the reference temperature $T_0$ needed in \eqref{eq:massHeatCoupling} should also be defined. A material-xml file in openCFS for LinFlow-heat coupling, e.g., for air, is

```
  <material name="Air"> 
    <flow>
       ...
    </flow>
    <heatConduction>
      <density>
        <linear>
          <real>1.225</real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real> 975.3</real>
        </linear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real> 25.18e-3</real>
          </isotropic>
        </linear>
      </heatConductivity>
      <refTemperature>
        <linear>
          <real> 294.3 </real>
        </linear>
      </refTemperature>
    </heatConduction>
  </material>
```

<!--### Analysis Types

This coupled PDE can be solved for the following analysis types:


* **Harmonic**

*not validated yet:*
 **Transient** 

-->
# References
\bibliography
