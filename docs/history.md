# History of openCFS

Although in the last years many commercial numerical simulation tools have been developed and are on the market, there is still a strong need for appropriate tools capable to simulate multi-field problems.
An implementation of a new partial differential equation (PDE) or physical coupling between PDEs into a commercial software, e.g. using provided interfaces via user routines, results in most cases in very inefficient numerical schemes.
For this reason, we have decided to develop our own tool, based on the Finite Element (FE) method.
The result of our effort is openCFS (before 2020 knows as CFS++ Coupled Field Simulations written in C++).

The development of openCFS was initiated by Manfred Kaltenbacher (back then) at the Department of Sensor Technology, Friedrich-Alexander University (FAU) Erlangen-Nuremberg, in the year 2000 and was then continuously improved within many Ph.D. projects supported by the German Research Council (DFG), the Austrian Research Council (FWF, FFG) and industry. Currently, the following main research groups further develop openCFS: 

* Research group of Manfred Kaltenbacher: 
  [Institute of Fundamentals and Theory in Electrical Engineering](https://www.tugraz.at/en/institutes/igte/home/), TU Graz, Austria
* Research group of Florian Toth: [Institute of Mechanics and Mechatronics](https://www.mec.tuwien.ac.at/mechanik_und_mechatronik_e325/), TU Wien, Austria 
* Research group of Michael Stingl: [Chair of Applied Mathematics](https://www.math.fau.de/kontinuierliche-optimierung/), Friedrich-Alexander University Erlangen-Nuremberg, Germany
* Research group of Reinhard Lerch: [Department of Sensor Technology](https://www.lse.tf.fau.de/), Friedrich-Alexander University Erlangen-Nuremberg, Germany

Here, you find a list of former and active developers and their main contributions (without demand of completeness):

* Escobar, Max: aeroacoustics
* Grabinger, Jens: nonconforming grids, aeroacoustics
* Hauck, Andreas: concepts, class structure, p-FEM
* Hüppem Andreas: aeroacoustics, spectral FEM, mixed FEM, concepts, class structure
* Kaltenbacher, Manfred : coupled PDEs, nonlinear PDEs, material modeling, hysteresis
* Link, Gerhard: fluid flow and coupling to mechanics
* Mohr, Michael: algebraic system, XML-structure
* Nierla, Michael: hysteresis in electromagnetics and piezoelectrics
* Roppert, Klaus: algebraic multigrid, multiharmonic analysis, non-conforming interfaces
* Schoder, Stefan: cfsdat, aeroacoustics, SNGR
* Seebacherm Philipp: topology optimization electromagnetics
* Shaposhnikov, Kirill: periodic boundary conditions
* Toth, Florian: buckling, modal analysis
* Triebenbacher, Simon: non-conforming grids, aeroacoustics, CFSDEPS
* Volk, Adrian: magnetostriction
* Fabian Wein: optimization
* Zhelezina, Elena: first implementations
* Zörner, Stefan: fluid-structure-acoustics 

